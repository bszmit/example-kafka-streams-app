package com.bszmit.ksapp.exceptionHandlers

import com.bszmit.ksapp.Metrics
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.streams.errors.{DefaultProductionExceptionHandler, ProductionExceptionHandler}

class MetricLoggingProductionExceptionHandler extends DefaultProductionExceptionHandler {
  override def handle(
      record: ProducerRecord[Array[Byte], Array[Byte]],
      exception: Exception,
  ): ProductionExceptionHandler.ProductionExceptionHandlerResponse = {
    Metrics.productionExceptionCounter.inc()
    super.handle(record, exception)
  }

}
