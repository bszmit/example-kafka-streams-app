package com.bszmit.ksapp.exceptionHandlers

import com.bszmit.ksapp.Metrics
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.streams.errors.{DeserializationExceptionHandler, LogAndContinueExceptionHandler}
import org.apache.kafka.streams.processor.ProcessorContext

class MetricLoggingDeserializationExceptionHandler extends LogAndContinueExceptionHandler {
  override def handle(
      context: ProcessorContext,
      record: ConsumerRecord[Array[Byte], Array[Byte]],
      exception: Exception,
  ): DeserializationExceptionHandler.DeserializationHandlerResponse = {
    Metrics.deserializationExceptionCounter.inc()
    super.handle(context, record, exception)
  }

}
