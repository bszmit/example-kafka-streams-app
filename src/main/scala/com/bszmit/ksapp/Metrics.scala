package com.bszmit.ksapp

import io.prometheus.client.Counter

object Metrics {
  val successCaseCounter = Counter
    .build()
    .name("success_case_counter")
    .help("success case")
    .register()

  val noEndErrCounter = Counter
    .build()
    .name("no_end_err_counter")
    .help("no_end error")
    .register()

  val unexpectedStartErrCounter = Counter
    .build()
    .name("unexpected_start_err_counter")
    .help("unexpected_start error")
    .register()

  val productionExceptionCounter = Counter
    .build()
    .name("production_exceptions_counter")
    .help("Number of production exceptions")
    .register()

  val deserializationExceptionCounter = Counter
    .build()
    .name("deserialization_exceptions_counter")
    .help("Number of deserialization exceptions")
    .register()

}
