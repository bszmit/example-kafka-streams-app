package com.bszmit.ksapp

import com.bszmit.ksapp.Event.{EndEvent, StartEvent}
import com.bszmit.ksapp.serdes.KafkaSerdes
import com.bszmit.ksapp.serdes.KafkaSerdes.instances
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.{JoinWindows, Named}
import org.apache.kafka.streams.processor.LogAndSkipOnInvalidTimestamp
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream._
import org.apache.kafka.streams.scala.serialization.Serdes
import org.slf4j.LoggerFactory
import Event.{EndEvent, StartEvent}
import Utils.KStreamOps

object DomainTopology {
  val logger = LoggerFactory.getLogger(this.getClass)

  def topology(config: AppConfig): Topology = {
    implicit val longSerde: Serde[Long]                                 = Serdes.longSerde
    implicit val eventSerde: Serde[Event]                               = instances.eventSerde
    implicit val startEventSerde: Serde[StartEvent]                     = KafkaSerdes.instances.startEventSerde
    implicit val endEventSerde: Serde[EndEvent]                         = KafkaSerdes.instances.endEventSerde
    implicit val startEndEventTupleSerde: Serde[(StartEvent, EndEvent)] = KafkaSerdes.instances.startEndEventTupleSerde
    implicit val errorSerde: Serde[Error]                               = KafkaSerdes.instances.errorSerde

    val builder = new StreamsBuilder()
    val source: KStream[Long, Event] = builder
      .stream(config.inputTopic)(
        Consumed
          .`with`[Long, Event](Topology.AutoOffsetReset.EARLIEST)
          .withName("source")
          .withTimestampExtractor(new LogAndSkipOnInvalidTimestamp),
      )
      .selectKey((_, event) => event.id) // it wasn't specified if keys on 'input' topic are already equal to event Id

    val startEventsStream: KStream[Long, StartEvent] = source.collect(
      { case e: StartEvent => e },
      Named.as("only-start-events"),
    )
    val endEventsStream: KStream[Long, EndEvent] = source.collect(
      { case e: EndEvent => e },
      Named.as("only-end-events"),
    )

    val window = JoinWindows.ofTimeDifferenceWithNoGrace(config.windowSize).before(java.time.Duration.ofMillis(0))

    val startWithMaybeEndStream: KStream[Long, (StartEvent, Option[EndEvent])] =
      startEventsStream.leftJoin(endEventsStream)((e1, e2) => (e1, Option(e2)), window)(StreamJoined.`with`)

    val startWithEndStream: KStream[Long, (StartEvent, EndEvent)] =
      startWithMaybeEndStream.collect({ case (e1, Some(e2)) => (e1, e2) }, Named.as("only-start-with-end-events"))
    val noEndStream: KStream[Long, StartEvent] =
      startWithMaybeEndStream.collect({ case (e1, None) => e1 }, Named.as("only-no-end"))

    val startEndMaybeStartStream: KStream[Long, (StartEvent, EndEvent, Option[StartEvent])] =
      startWithEndStream.leftJoin(startEventsStream)(
        { case ((start, end), maybeNextStart) => (start, end, Option(maybeNextStart)) },
        window,
      )(StreamJoined.`with`)

    val unexpectedStartStream: KStream[Long, (StartEvent, EndEvent, StartEvent)] =
      startEndMaybeStartStream.collect(
        { case (start1, end, Some(start2)) => (start1, end, start2) },
        Named.as("only-unexpected-start"),
      )
    val successCaseStream: KStream[Long, (StartEvent, EndEvent)] =
      startEndMaybeStartStream.collect({ case (start1, end, None) => (start1, end) }, Named.as("only-success-case"))

    // ERROR
    noEndStream
      .mapValues(e => Error(e.id, "no_end"))
      .peek((_, _) => Metrics.noEndErrCounter.inc())
      .peek((_, e) => logger.debug("Producing no_end error {} to topic {}", e, config.errorTopic))
      .to(config.errorTopic)(Produced.`with`)
    unexpectedStartStream
      .mapValues { case (e, _, _) => Error(e.id, "unexpected_start") }
      .peek((_, _) => Metrics.unexpectedStartErrCounter.inc())
      .peek((_, e) => logger.debug("Producing unexpected_start error {} to topic {}", e, config.errorTopic))
      .to(config.errorTopic)(Produced.`with`)

    // OK
    successCaseStream
      .mapValues((key, _) => key)
      .peek((_, _) => Metrics.successCaseCounter.inc())
      .peek((_, e) => logger.debug("Producing success_case event {} to topic {}", e, config.outputTopic))
      .to(config.outputTopic)(Produced.`with`)

    builder.build()
  }

}
