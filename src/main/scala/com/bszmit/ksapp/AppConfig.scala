package com.bszmit.ksapp

import com.typesafe.config.ConfigFactory

final case class AppConfig(
    applicationId: String,
    kafkaBrokers: String,
    inputTopic: String,
    outputTopic: String,
    errorTopic: String,
    windowSize: java.time.Duration,
    metricHttpHost: String,
    metricHttpPort: Int,
)

object AppConfig {
  def load(): AppConfig = {
    val config = ConfigFactory.load()

    val inputTopic     = config.getString("kafka-input-topic")
    val outputTopic    = config.getString("kafka-output-topic")
    val errorTopic     = config.getString("kafka-error-topic")
    val brokers        = config.getString("kafka-brokers")
    val applicationId  = config.getString("application-id")
    val windowSize     = java.time.Duration.ofSeconds(config.getInt("window-size-sec"))
    val metricHttpHost = config.getString("metric-http-host")
    val metricHttpPort = config.getInt("metric-http-port")

    AppConfig(applicationId, brokers, inputTopic, outputTopic, errorTopic, windowSize, metricHttpHost, metricHttpPort)
  }
}
