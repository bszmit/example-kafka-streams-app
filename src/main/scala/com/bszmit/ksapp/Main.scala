package com.bszmit.ksapp

import com.bszmit.ksapp.exceptionHandlers.{MetricLoggingDeserializationExceptionHandler, MetricLoggingProductionExceptionHandler}
import io.prometheus.client.exporter.HTTPServer
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig, Topology}
import org.slf4j.LoggerFactory

import java.util.Properties
import java.util.concurrent.CountDownLatch
import scala.jdk.CollectionConverters.MapHasAsJava
import scala.util.{Failure, Success, Try}

object Main extends App {
  val logger = LoggerFactory.getLogger(this.getClass)

  val config = AppConfig.load()

  val props = new Properties()
  props.putAll(
    Map(
      StreamsConfig.APPLICATION_ID_CONFIG                                  -> config.applicationId,
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG                               -> config.kafkaBrokers,
      StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG -> classOf[MetricLoggingDeserializationExceptionHandler],
      StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG      -> classOf[MetricLoggingProductionExceptionHandler],
    ).asJava,
  )

  val t: Topology = DomainTopology.topology(config)
  val streams     = new KafkaStreams(t, props)
  val latch       = new CountDownLatch(1)

  logger.info(s"Running app with config: {}", config)

  Runtime.getRuntime.addShutdownHook(new Thread("streams-shutdown-hook") {
    override def run(): Unit = {
      streams.close()
      latch.countDown()
    }
  })

  Try {
    new HTTPServer.Builder().withHostname(config.metricHttpHost).withPort(config.metricHttpPort).build()
    streams.start()
    latch.await()
  } match {
    case Failure(e) =>
      logger.error("Unexpected end of application", e)
      java.lang.System.exit(1)
    case Success(_) => java.lang.System.exit(0)
  }

}
