package com.bszmit.ksapp

import org.apache.kafka.streams.scala.kstream.{KStream, Named}

object Utils {

  implicit class KStreamOps[K, V](ks: KStream[K, V]) {
    def collect[V1](pf: PartialFunction[V, V1], named: Named): KStream[K, V1] =
      ks.flatMapValues(
        e => if (pf.isDefinedAt(e)) List(pf.apply(e)) else List.empty,
        named,
      )
  }

}
