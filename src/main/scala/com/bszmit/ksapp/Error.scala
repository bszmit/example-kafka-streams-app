package com.bszmit.ksapp

final case class Error(id: Long, reason: String)
