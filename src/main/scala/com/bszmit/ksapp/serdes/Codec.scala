package com.bszmit.ksapp.serdes

import com.bszmit.ksapp
import com.bszmit.ksapp.{Error, Event}
import com.bszmit.ksapp.Event.{EndEvent, StartEvent}
import Event.{EndEvent, StartEvent}

import scala.util.{Failure, Try}

trait Codec[A, B] {
  def encode(a: A): B
  def decode(b: B): Try[A]
  final def decodeUnsafe(b: B): A = decode(b).get
}

object Codec {
  implicit class CodecOps[A, B](c: Codec[A, B]) {
    def comap[A1](c1: Codec[A1, A]): Codec[A1, B] = new Codec[A1, B] {
      def encode(a: A1): B      = c.encode(c1.encode(a))
      def decode(b: B): Try[A1] = c.decode(b).flatMap(c1.decode)
    }
  }

  // type arguments in Codec have to be invariant, since they're used both in A => B and B => A.
  // I'm using this function to generate codec for subtypes
  def forSubtype[A1 <: A, A]: Codec[A1, A] = new Codec[A1, A] {
    def encode(a: A1): A      = a
    def decode(b: A): Try[A1] = Try(b.asInstanceOf[A1])
  }

  def tupleCodec[A: Codec[*, String], B: Codec[*, String]]: Codec[(A, B), String] = new Codec[(A, B), String] {
    // we should add escaping logic here, if A or B would contain ';' character
    def encode(x: (A, B)): String = {
      val (a, b)   = x
      val encodedA = implicitly[Codec[A, String]].encode(a)
      val encodedB = implicitly[Codec[B, String]].encode(b)
      s"$encodedA;$encodedB"
    }

    def decode(s: String): Try[(A, B)] =
      s.split(";") match {
        case Array(a, b) =>
          for {
            decodedA <- implicitly[Codec[A, String]].decode(a)
            decodedB <- implicitly[Codec[B, String]].decode(b)
          } yield (decodedA, decodedB)
        case _ => Failure(new Exception(s"Failed to decode tuple from $s"))
      }
  }

  object instances {
    val eventStringCodec: Codec[Event, String] = new Codec[Event, String] {
      def encode(a: Event): String = {
        val t = a match {
          case _: Event.StartEvent => "START"
          case _: Event.EndEvent   => "END"
        }
        s"${a.id},$t"
      }

      def decode(b: String): Try[Event] = {
        val ex = new Exception(s"Failed decoding Event from $b")
        Try {
          b.split(",") match {
            case Array(id, "START") => Event.StartEvent(id.toLong)
            case Array(id, "END")   => Event.EndEvent(id.toLong)
            case _                  => throw ex
          }
        }.recoverWith { case _ => Failure(ex) }
      }
    }

    val startEventStringCodec: Codec[StartEvent, String]                = eventStringCodec.comap(Codec.forSubtype)
    val endEventStringCodec: Codec[EndEvent, String]                    = eventStringCodec.comap(Codec.forSubtype)
    val startEndTupleStringCodec: Codec[(StartEvent, EndEvent), String] = Codec.tupleCodec(startEventStringCodec, endEventStringCodec)

    val errorStringCodec: Codec[ksapp.Error, String] = new Codec[ksapp.Error, String] {
      def encode(a: ksapp.Error): String = s"${a.id},${a.reason}"
      def decode(b: String): Try[ksapp.Error] = {
        val ex = new Exception(s"Failed decoding Event from $b")
        Try {
          b.split(",") match {
            case Array(id, reason) => Error(id.toLong, reason)
            case _                 => throw ex
          }
        }.recoverWith { case _ => Failure(ex) }
      }
    }
  }

}
