package com.bszmit.ksapp.serdes

import com.bszmit.ksapp
import com.bszmit.ksapp.{Error, Event}
import com.bszmit.ksapp.Event.{EndEvent, StartEvent}
import org.apache.kafka.common.serialization.{Deserializer, Serde, Serializer}
import org.apache.kafka.streams.scala.serialization.Serdes
import Event.{EndEvent, StartEvent}

object KafkaSerdes {
  private implicit class serdeOps[T](s: Serde[T]) {
    def comap[U](codec: Codec[U, T]): Serde[U] = new Serde[U] {
      def serializer(): Serializer[U] = (topic: String, data: U) => s.serializer().serialize(topic, codec.encode(data))

      def deserializer(): Deserializer[U] = (topic: String, data: Array[Byte]) =>
        codec.decodeUnsafe(s.deserializer().deserialize(topic, data))
    }
  }

  object instances {
    val eventSerde: Serde[Event]                               = Serdes.stringSerde.comap(Codec.instances.eventStringCodec)
    val startEventSerde: Serde[StartEvent]                     = Serdes.stringSerde.comap(Codec.instances.startEventStringCodec)
    val endEventSerde: Serde[EndEvent]                         = Serdes.stringSerde.comap(Codec.instances.endEventStringCodec)
    val startEndEventTupleSerde: Serde[(StartEvent, EndEvent)] = Serdes.stringSerde.comap(Codec.instances.startEndTupleStringCodec)
    val errorSerde: Serde[ksapp.Error]                               = Serdes.stringSerde.comap(Codec.instances.errorStringCodec)
  }

}
