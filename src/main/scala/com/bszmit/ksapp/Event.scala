package com.bszmit.ksapp

sealed trait Event {
  val id: Long
}

object Event {
  final case class StartEvent(id: Long) extends Event
  final case class EndEvent(id: Long)   extends Event
}
