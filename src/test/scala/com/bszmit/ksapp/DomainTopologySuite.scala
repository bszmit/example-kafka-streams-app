package com.bszmit.ksapp

import com.bszmit.ksapp.serdes.KafkaSerdes
import org.apache.kafka.streams.scala.serialization.Serdes
import org.apache.kafka.streams.{TestInputTopic, TestOutputTopic, TopologyTestDriver}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import Event.{EndEvent, StartEvent}

import scala.jdk.CollectionConverters.ListHasAsScala
import java.time.Duration

class DSL(t: TopologyTestDriver, i: TestInputTopic[Long, Event]) {
  def send(e: Event): DSL = {
    i.pipeInput(e.id, e)
    this
  }
  def advance(d: Duration): DSL = {
    i.advanceTime(d)
    t.advanceWallClockTime(d)
    this
  }

}
class DomainTopologySuite extends AnyFlatSpec with should.Matchers {
  val config = AppConfig.load()

  val t = DomainTopology.topology(config)

  val testDriver = new TopologyTestDriver(t)

  val inputTopic =
    testDriver.createInputTopic(config.inputTopic, Serdes.longSerde.serializer(), KafkaSerdes.instances.eventSerde.serializer())
  val testOutputTopic = testDriver.createOutputTopic(config.outputTopic, Serdes.longSerde.deserializer(), Serdes.longSerde.deserializer())
  val testErrorTopic =
    testDriver.createOutputTopic(config.errorTopic, Serdes.longSerde.deserializer(), KafkaSerdes.instances.errorSerde.deserializer())

  val dsl = new DSL(testDriver, inputTopic)
  implicit class inputOps[V](t: TestOutputTopic[_, V]) {
    def readValuesToScalaList(): List[V] = t.readValuesToList().asScala.toList
  }


  "A topology" should "send event's id to output topic for success_case" in {
    val id = 1
    dsl
      .send(StartEvent(id))
      .advance(Duration.ofSeconds(2))
      .send(EndEvent(id))
      .advance(Duration.ofSeconds(61))
      .send(StartEvent(id))

    testOutputTopic.readValuesToScalaList().filter(_ == id) shouldEqual List(1)
    testErrorTopic.readValuesToScalaList().filter(_.id == id) shouldEqual List.empty
  }

  it should "send error event to error topic in case of unexpected_start error" in {
    val id = 2
    dsl
      .send(StartEvent(id))
      .advance(Duration.ofSeconds(2))
      .send(EndEvent(id))
      .advance(Duration.ofSeconds(10))
      .send(StartEvent(id))

    testOutputTopic.readValuesToScalaList().filter(_ == id) shouldEqual List.empty
    testErrorTopic.readValuesToScalaList().filter(_.id == id) shouldEqual List(Error(id, "unexpected_start"))
  }

  it should "send error event to error topic in case of no_end error" in {
    val id = 3
    dsl
      .send(StartEvent(id))
      .advance(Duration.ofSeconds(61))
      .send(EndEvent(id))

    testOutputTopic.readValuesToScalaList().filter(_ == id) shouldEqual List.empty
    testErrorTopic.readValuesToScalaList().filter(_.id == id) shouldEqual List(Error(id, "no_end"))
  }

}
