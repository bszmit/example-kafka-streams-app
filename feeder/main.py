import os
import threading
from concurrent.futures import ThreadPoolExecutor

import numpy
import time
from kafka import KafkaProducer


def send_to_kafka(producer, topic, key, value):
    value = "{},{}".format(key, value)
    producer.send(topic, value=value, key=key)
    print('sent event {}:{} to {}'.format(key, value, topic))


def keep_sending_events(producer, topic, id):
    while True:
        send_to_kafka(producer, topic, id, "START")
        delay_sec = min(90, max(0, numpy.random.normal(30, 25)))
        time.sleep(delay_sec)

        send_to_kafka(producer, topic, id, "END")
        delay_sec = min(90, max(0, numpy.random.normal(30, 25)))
        time.sleep(delay_sec)


if __name__ == '__main__':
    KAFKA_BROKER = os.getenv('KAFKA_BROKER')
    INPUT_TOPIC = os.getenv('INPUT_TOPIC')
    NR_OF_USERS = int(os.getenv('NR_OF_USERS'))

    producer = None

    for i in range(0, 10):
        try:
            producer = KafkaProducer(
                bootstrap_servers=KAFKA_BROKER,
                key_serializer=lambda x: x.to_bytes(8, byteorder='big'),
                value_serializer=str.encode
            )
            break
        except Exception:
            print("producer not ready")
            time.sleep(5)
            continue
    print(producer)

    with ThreadPoolExecutor() as executor:
        threads = [threading.Thread(target=keep_sending_events, args=(producer, INPUT_TOPIC, event_id)) for event_id in
                   range(1, NR_OF_USERS)]
        for t in threads:
            t.start()
