ThisBuild / version := "1.0.0"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "example-kafka-streams-app",
    libraryDependencies ++= Seq(
      "org.apache.kafka"        %% "kafka-streams-scala"      % "3.2.1",
      "org.apache.kafka"         % "kafka-streams-test-utils" % "3.2.1"  % Test,
      "org.apache.logging.log4j" % "log4j-api"                % "2.18.0",
      "org.apache.logging.log4j" % "log4j-core"               % "2.18.0",
      "org.apache.logging.log4j" % "log4j-slf4j-impl"         % "2.18.0",
      "io.prometheus"            % "simpleclient"             % "0.16.0",
      "io.prometheus"            % "simpleclient_httpserver"  % "0.16.0",
      "com.typesafe"             % "config"                   % "1.4.2",
      "org.scalatest"           %% "scalatest"                % "3.2.13" % Test,
    ),
    assemblyMergeStrategy in assembly := {
      case "module-info.class" => MergeStrategy.discard
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },
  )

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)
