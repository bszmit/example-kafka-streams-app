# Recruitment task
## Run app:  
`sbt run`  

By default, app will expect kafka brokers to be available at `localhost:9092`. You can override it by setting env var `KAFKA_BROKERS`. Look into `applicatino.conf` for more configuration options.

### Dockerized Example
To run complete example with app, kafka and feeder run:
`sbt assembly && docker compose build && docker compose up`

This will start zookeeper and kafka instances, feeder and this app.
Feeder is used to generate random traffic to 'input' topic.

You can observe effects by:
- looking at metrics: `curl localhost:8080/metrics`
- seeing what traffic is sent to 'input' topic: `kafka-console-consumer --bootstrap-server localhost:9092 --topic input --from-beginning`
- seeing what traffic is sent to 'output' topic: `kafka-console-consumer --bootstrap-server localhost:9092 --topic output --from-beginning -property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer`
- seeing what traffic is sent to 'error' topic: `kafka-console-consumer --bootstrap-server localhost:9092 --topic error --from-beginning`

### Glossary
Terminology used across codebase:
- `success_case` - expected events timing. Start event followed by an End event within 1 minute and no further Start event for at least 1 more minute
- `no_end` error - happens when Start event isn't followed by End event within 1 minute
- `unexpected_start` error - happens when Start event is followed by an End event within 1 minute but another Start event happens within 1 minute
### Metrics
Metrics are available under: `GET localhost:8080/metrics`
#### Available metrics:
- `success_case_counter` - counts `success_case` cases
- `no_end_err_counter` - counts `no_end` cases
- `unexpected_start_err_counter` - counts `unexpected_start` cases
- `production_exceptions` - counts problems with producing event to kafka topics
- `deserialization_exceptions` - counts problems with deserializing events from kafka topics  

Next steps would be setting up Prometheus instance and presenting metrics to Grafana dashboard
