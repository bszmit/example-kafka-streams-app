FROM openjdk:8-jdk

ADD target/scala-2.13/example-kafka-streams-app-assembly-1.0.0.jar /application/example-kafka-streams-app-assembly.jar

EXPOSE 8080

CMD exec java -jar /application/example-kafka-streams-app-assembly.jar

